import java.util.Scanner;

public class TaxiCostCalculator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введіть відстань у кілометрах: ");
        double distance = input.nextDouble();

        System.out.println("Введіть час у хвилинах: ");
        double time = input.nextDouble();

        System.out.println("Зараз ніч? (Так/Ні) ");
        String night = input.next();

        boolean isNight;
        if (night.equalsIgnoreCase("Так")) {
            isNight = true;
        } else {
            isNight = false;
        }
        Trip trip = new Trip(distance, time, isNight);

        System.out.println("Загальна вартість поїздки " + trip.getCost());

        input.close();
    }
}
