public class Trip {
    double distance;
    double time;
    boolean isNight;

    public Trip(double distance, double time, boolean isNight) {
        this.distance = distance;
        this.time = time;
        this.isNight = isNight;
    }

    public double getCost() {
        double cost;

        if (this.isNight) {
            cost = this.distance * 1.4 + this.time * 0.4 + 4;
        } else {
            cost = this.distance * 1.2 + this.time * 0.4;
        }
        return cost;
    }
}
