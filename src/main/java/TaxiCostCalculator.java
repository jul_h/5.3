import java.util.Scanner;

public class TaxiCostCalculator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введіть відстань у кілометрах: ");
        double distance = input.nextDouble();

        System.out.println("Введіть час у хвилинах: ");
        double time = input.nextDouble();

        System.out.println("Зараз ніч? (Так/Ні) ");
        String night = input.next();

        double cost;

        if (night.equalsIgnoreCase("Так")) {
            cost = distance * 1.4 + time * 0.4 + 4;
        } else {
            cost = distance * 1.2 + time * 0.4;
        }
        System.out.println("Загальна вартість поїздки " + cost);

        input.close();
    }
}

